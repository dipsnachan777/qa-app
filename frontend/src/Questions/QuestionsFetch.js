import React from 'react';
import axios from "axios";


//     static getJSONAsync(){
//     return fetch('http://localhost:8081/')
//         .then(response => response.json())
//         .then(data => this.setState({ data }));

//     }



// export default getJSONAsync;
export default async getUser => {
    const resp = await axios.get("https://reqres.in/api/users/2");
    return resp.data;
  };
  export const doAdd = (a, b, callback) => {
    callback(a + b);
  };