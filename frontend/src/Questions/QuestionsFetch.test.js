import React from "react";
import mockAxios from "axios";
import getUser, { doAdd } from "./QuestionsFetch";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

//jest.mock('axios');

// test('Should fetch users', async () => {
//     const wrapper = shallow(<QuestionsFetch />);
//     const wrapperInstance = wrapper.instance();
//     const resp = {data: [{name: 'Bob'}]};
//     //axios.get.mockResolvedValue(resp);
//     //console.log(QuestionsFetch.getJSONAsync());
//     const data = await wrapperInstance.getJSONAsync();
//     console.log(data);
//     return data;
// });

// test("Test for String return", () => {
//     console.log(QuestionsFetch.returnString());
// })

test("It calls axios api and returns user", async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: {
        id: 2,
        first_name: "Janet",
        last_name: "Weaver",
        avatar:
          "https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg"
      }
    })
  );
  const user = await getUser();
  expect(mockAxios.get).toHaveBeenCalledTimes(1);
    //console.log("usr data", user);
});

test("calls callback with arguments added", () => {
    const mockCallback = jest.fn();
    doAdd(1, 2, mockCallback);
    expect(mockCallback).toHaveBeenCalledWith(3);
  });
